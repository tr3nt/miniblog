const mix = require('laravel-mix');


mix
    .setPublicPath('public/dist')
    .js('resources/js/app.js', 'app.js')
    .js('resources/main.js', 'main.js')
    .sass('resources/sass/app.scss', 'app.css')
    .sass('resources/sass/vuetify.scss', 'vuetify.css')
    .vue();
