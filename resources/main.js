import Vue from 'vue'
/** Vista Principal */
import App from './App'
/** Vuetify */
import vuetify from './js/vuetify'
/** Vue Router */
import router from './js/router'
/** Vuex */
import store from './js/store'
/** Event Bus */
window.EventBus = new Vue()

/** Componentes */
import Menu from './components/Menu'
Vue.component('v-menu', Menu)
import Loader from './components/Loader'
Vue.component('v-loader', Loader)
import Dialog from './components/Dialog'
Vue.component('v-message', Dialog)

/** Nueva instancia de Vue */
const app = new Vue({
    render: h => h(App),
    vuetify,
    router,
    store
}).$mount('#app')
