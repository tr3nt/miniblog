import Vue from 'vue'
import Vuex from 'vuex'
import Axios from 'axios'
import system from './modules/system'
import article from './modules/article'
import comment from './modules/comment'

Vue.use(Vuex);
Axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

export default new Vuex.Store({
    actions: {
        /** Acciones AJAX para el servicio REST */
        GET: async ({ commit, dispatch }, data) => {

            /** Iniciar la animación AJAX Loader */
            dispatch('loaderOn')

            /** Obtener los datos necesarios */
            let [url, mutation, params] = data,
                response

            try {
                /** Hacer el llamado GET por AJAX */
                if (typeof params == 'object')
                    response = await Axios.get(url, { params })
                else
                    response = await Axios.get(url)
                /** Cargar los datos en una Mutation */
                commit(mutation, response.data)
            } catch (error) {
                /** Enviar el mensaje de Error */
                if (typeof error.response.data.message == 'string')
                    dispatch('message', error.response.data.message)
                else
                    dispatch('message', error.response.data)
            } finally {
                /** Cerrar la animación de AJAX Loader */
                dispatch('loaderOff')
            }
        },

        QUERY: async ({ commit, dispatch }, data) => {

            /** Iniciar la animación AJAX Loader */
            dispatch('loaderOn')

            /** Obtener los datos necesarios */
            let [url, params, request] = data,
                fileHead = { headers: { 'Content-Type': 'multipart/form-data' }},
                response

            try {
                /** Hacer el llamado por AJAX según su REQUEST */
                switch (request) {
                    case 'file':
                        response = await Axios.post(url, params, fileHead)
                        break
                    case 'put':
                        response = await Axios.put(url, params)
                }
                /** Cargar la respuesta en un Modal de mensaje */
                EventBus.$emit(`${url}-${request}`, true)
                dispatch('message', response.data)
            } catch (error) {
                /** Enviar el mensaje de Error */
                if (typeof error.response.data.message == 'string')
                    dispatch('message', error.response.data.message)
                else
                    dispatch('message', error.response.data)
            } finally {
                /** Cerrar la animación de AJAX Loader */
                dispatch('loaderOff')
            }
        }
    },
    modules: {
        system,
        article,
        comment
    }
});
