export default {
    state: {
        comment: {
            id_article: 0,
            id_user: 0,
            comment: null,
            rating: 0
        }
    },
    getters: {
        comment: state => {
            return state.comment
        }
    },
    mutations: {
        setComment: (state, comment) => {
            state.comment.comment = comment
        },
        setCommentIdArticle: (state, id_article) => {
            state.comment.id_article = id_article
        },
        setCommentRating: (state, rating) => {
            state.comment.rating = rating
        }
    },
    actions: {
        /** Crear comentario */
        saveComment: ({ state, dispatch }, id_user) => {
            state.comment.id_user = id_user
            dispatch('QUERY', ['api/comments', state.comment, 'put'])
        },
        cleanComment: ({ state }) => {
            state.comment.comment = null
            state.comment.rating = 0
        }
    }
}
