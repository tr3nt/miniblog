export default {
    state: {
        user: {},
        loader: false,
        modal: {
            display: false,
            message: 'Hola Mensajero'
        }
    },
    mutations: {
        setUser: (state, user) => {
            state.user = user
        },
        setLoader: (state, loader) => {
            state.loader = loader
        },
        setDialog: (state, dialog) => {
            let [display, message] = dialog
            state.modal.display = display
            state.modal.message = message
        }
    },
    actions: {
        /** Obtener los datos del usuario logueado */
        getUser: ({ dispatch }) => {
            dispatch('GET', ['user', 'setUser'])
        },

        /** Animación de Loader para AJAX */
        loaderOn: ({ commit }) => {
            commit('setLoader', true)
        },
        loaderOff: ({ commit }) => {
            commit('setLoader', false)
        },

        /** Modal con mensajes del sistema */
        message: ({ commit }, message) => {
            commit('setDialog', [true, message])
        },
        closeDialog: ({ commit }, display) => {
            commit('setDialog', [display, ''])
        }
    }
}
