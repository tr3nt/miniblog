export default {
    state: {
        listArticles: [],
        article: {
            user: {
                name: ''
            },
            comments: []
        },
        articleToSave: {
            title: null,
            picture: null,
            article: null
        }
    },
    getters: {
        /** Lista de artículos */
        articles: state => {
            return state.listArticles
        },
        /** Artículo elegido */
        article: state => {
            return state.article
        },
        /** Artículo a crear o actualizar */
        articleToSave: state => {
            return state.articleToSave
        }
    },
    mutations: {
        /** Cargar los artículos a la lista */
        setArticles: (state, listArticles) => {
            state.listArticles = listArticles
        },
        /** Cargar el artículo elegido */
        setArticle: (state, article) => {
            state.article = article
        },
        setArticleComments: (state, comments) => {
            state.article.comments = comments
        },
        /** Cargar los input form del artículo */
        setArticleTitle: (state, title) => {
            state.articleToSave.title = title
        },
        setArticlePicture: (state, picture) => {
            state.articleToSave.picture = picture
        },
        setArticleArticle: (state, article) => {
            state.articleToSave.article = article
        }
    },
    actions: {
        /** Obtener la lista de artículos por AJAX */
        getArticles: ({ dispatch }) => {
            dispatch('GET', ['api/articles', 'setArticles'])
        },
        /** Crear un artículo nuevo por AJAX */
        saveArticle: ({ state, dispatch }, id_user) => {
            let formData = new FormData();
            formData.append('id_user', id_user);
            formData.append('title', state.articleToSave.title);
            formData.append('picture', state.articleToSave.picture);
            formData.append('article', state.articleToSave.article);
            dispatch('QUERY', ['api/articles', formData, 'file'])
        },
        getArticleComments: ({ dispatch }, id_article) => {
            dispatch('GET', ['api/comments', 'setArticleComments', { id_article }])
        },
        cleanArticleToSave: ({ state }) => {
            state.articleToSave = {
                title: null,
                picture: null,
                article: null
            }
        }
    }
}
