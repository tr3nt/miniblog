import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home'

const routes = [
    {
        /** Ruta principal con vista Home.vue */
        path: '/home',
        component: Home
    }
];

Vue.use(VueRouter);

export default new VueRouter({
    /** Modo sin Hashtag */
    mode: 'history',
    routes
});
