<?php

use App\Http\Controllers\AppController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => 'web'], function () {
    Route::get('/user', [AppController::class, 'getUser']);
});

Route::get('/', function () {
    return view('welcome');
});

Route::get('/logout', [HomeController::class, 'logout'])->name('logout');

Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');
