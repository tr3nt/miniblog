<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AppController;
use App\Http\Controllers\HomeController;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/** Artículos */
Route::get('articles', [AppController::class, 'getArticles']);
Route::post('articles', [AppController::class, 'addArticle']);

/** Comentarios */
Route::get('comments', [AppController::class, 'getCommentsById']);
Route::put('comments', [AppController::class, 'addComment']);
