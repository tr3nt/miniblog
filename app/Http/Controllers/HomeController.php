<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    /**
     * Obtener el usuario logueado
     */
    public function getUser(): JsonResponse
    {
        return response()->json(Auth::check());
    }

    /**
     * Desconectarse de la aplicación
     */
    public function logout()
    {
        Auth::logout();
        return redirect()->route('login');
    }
}
