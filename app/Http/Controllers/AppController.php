<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Comment;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use SebastianBergmann\Environment\Console;

class AppController extends Controller
{
    public function index()
    {
        return view('index');
    }

    /**
     * Obtener Lista de todos los artículos
     * @return JsonResponse
     */
    public function getArticles(): JsonResponse
    {
        try {
            $articles = Article::with('user')
                ->orderBy('id', 'desc')
                ->get();
            return response()->json($articles);
        } catch (QueryException $e) {
            return response()->json($e->getMessage(), 500);
        }
    }
    /**
     * Crear un artículo nuevo con validación de servidor
     * @return JsonResponse
     */
    public function addArticle(Request $request)
    {
        $params = $request->all();
        $validator = Validator::make($params, [
            'id_user' => 'required|integer',
            'title' => 'required|string',
            'article' => 'required|string'
        ], [
            'id_user.required' => 'El Autor es obligatorio',
            'id_user.integer' => 'El Autor es inválido',
            'title.required' => 'El títlo es obligatorio',
            'article.required' => 'El Artículo es obligatorio'
        ]);
        /** Validar campos */
        if ($validator->fails()) {
            return response()->json($validator->errors()->all(), 500);
        }
        try {
            /** Subir la imagen en caso de elegir una */
            if ($params["picture"] !== 'null' && $params["picture"]->isValid()) {
                $name = $params["picture"]->getClientOriginalName();
                $params["picture"]->move(base_path("public/dist"), $name);
                $params["picture"] = $name;
            } else {
                $params["picture"] = 'nopic.png';
            }
            /** Crear artículo */
            Article::create($params);
            return response()->json('Artículo creado correctamente', 201);
        } catch (QueryException $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

    /**
     * Obtener Lista de todos los comentarios de un artículo por su ID
     * @return JsonResponse
     */
    public function getCommentsById(Request $request): JsonResponse
    {
        try {
            $comments = Comment::whereIdArticle($request->id_article)
                ->orderBy('id', 'desc')
                ->get();
            return response()->json($comments);
        } catch (QueryException $e) {
            return response()->json($e->getMessage(), 500);
        }
    }
    /**
     * Crear un comentario sobre un artículo con validación de servidor
     * @return JsonResponse
     */
    public function addComment(Request $request)
    {
        $params = $request->all();
        $validator = Validator::make($params, [
            'id_user' => 'required|integer',
            'id_article' => 'required|integer',
            'comment' => 'required|string'
        ], [
            'id_user.required' => 'El Autor es obligatorio',
            'id_user.integer' => 'Autor inválido',
            'id_article.required' => 'El Artículo es obligatorio',
            'id_article.integer' => 'Artículo inválido',
            'comment.required' => 'El Comentario es obligatoria'
        ]);
        /** Validar campos */
        if ($validator->fails()) {
            return response()->json($validator->errors()->all(), 500);
        }

        try {
            /** Crear artículo */
            Comment::create($params);
            return response()->json('Comentario creado correctamente', 201);
        } catch (QueryException $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

    /**
     * Obtener el usuario logueado
     */
    public function getUser(): JsonResponse
    {
        return response()->json(Auth::user());
    }
}
