<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    /**
     * Campos de la tabla Artículo
     * @var array
     */
    protected $fillable = [
        'id_user',
        'title',
        'article',
        'picture'
    ];
    /**
     * Usuario creador del artículo
     * @var array
     */
    protected $appends = [
        'user',
        'comments'
    ];

    /**
     * Relaciones de tablas
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'id_user', 'id');
    }
    public function getUserAttribute()
    {
        return $this->user()->first();
    }

    public function comments()
    {
        return $this->hasMany('App\Models\Comment', 'id_article', 'id');
    }
    public function getCommentsAttribute()
    {
        return $this->comments()->get();
    }
}
