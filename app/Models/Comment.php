<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    /**
     * Campos de la tabla Comentarios
     * @var array
     */
    protected $fillable = [
        'id_article',
        'id_user',
        'comment',
        'rating'
    ];

    /**
     * Usuario creador del comentario
     * @var array
     */
    protected $appends = [
        'user'
    ];

    /**
     * Relaciones de tablas
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'id_user', 'id');
    }
    public function getUserAttribute()
    {
        return $this->user()->first();
    }
}
