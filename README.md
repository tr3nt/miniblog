# Mini Blog

## Librerías

-   Laravel 8.0 - Servicios
-   Vue 2 - Backend
-   Vuex - Backend
-   MySQL - Persistencia
-   Vuetify 2 - Frontend
-   Node 18 - Compilador
-   Composer - Gestor de librerías

## Instalación

Descargar el repositorio

```bash
git clone https://tr3nt@bitbucket.org/tr3nt/miniblog.git
```

Instalar Laravel

```bash
composer install
```

Crear el archivo .env y asignar los valores correctos del servidor MySQL

```bash
cp .env-example .env
```

Crear la clave de la aplicación

```bash
php artisan key:generate
```

Correr las migraciones

```bash
php artisan migrate
```

Correr los seeders

```bash
php artisan db:seed
```

Se crearán 2 usuarios de prueba:

-   user: usuario1@gmail.com | pass: 12345678
-   user: usuario2@gmail.com | pass: 12345678

En la aplicación se podrán crear más usuarios.

## Hecho por

Esaim Nájera Mondragón para empresas reclutadoras
