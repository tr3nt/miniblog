<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_article')->unsigned();
            $table->bigInteger('id_user')->unsigned();
            $table->mediumText('comment');
            $table->integer('rating');
            $table->timestamps();
        });

        /** LLaves foráneas */
        Schema::table('comments', function (Blueprint $table) {
            $table->foreign('id_article')->references('id')->on('articles');
            $table->foreign('id_user')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
