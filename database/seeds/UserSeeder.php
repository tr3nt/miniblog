<?php

use Faker\Generator as Faker;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\Article;
use App\Models\Comment;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Usuarios de prueba con 2 artículos y 1 comentario
         * @var array
         */
        $users = [
            [
                'name' => 'Mario López',
                'email' => 'usuario1@gmail.com',
                'password' => Hash::make('12345678')
            ], [
                'name' => 'Alejandra Solís',
                'email' => 'usuario2@gmail.com',
                'password' => Hash::make('12345678')
            ]
        ];

        $second_user = false;

        foreach ($users as $user) {

            /** Crear el Usuario */
            $user = User::create($user);

            /** Crear el artículo del Usuario */
            $article = Article::create([
                'id_user' => $user->id,
                'title' => 'Lorem ipsum dolor sit amet',
                'article' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
                'picture' => 'nopic.png'
            ]);

            /** Crear un comentario del segundo Usuario sobre el primer Artículo */
            if ($second_user) {
                Comment::create([
                    'id_article' => --$article->id,
                    'id_user' => $user->id,
                    'comment' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt',
                    'rating' => 3
                ]);
            } else {
                $second_user = $user->id;
            }
        }
    }
}
